'use strict';

var Liftoff = require('liftoff'),
    Mout = require('mout'),
    Nopt = require('nopt'),
    Winston = require("winston"),
    Figlet = require('figlet'),
    Fs = require('fs-extra'),
    ReadYaml = require('read-yaml-promise'),
    Async = require("async"),
    Path = require('path'),
    Slash = require('slash');
/* inject:commons:js */
var Format = Winston.format;

var logger = Winston.createLogger({
  format: Format.combine(Format(function (info) {
    info.level = info.level.toUpperCase();
    return info;
  })(), Format.colorize(), Format.timestamp({
    format: 'HH:mm:ss'
  }), Format.splat(), Format.simple(), Format.printf(function (info) {
    return '[' + info.timestamp + '] ' + info.level + ': ' + info.message;
  })),
  transports: [new Winston.transports.Console()]
});
function CagWorkspace() {
  var WORKSPACE_DIR = 'workspaces',
      CAG_PLATFORM_DIR = 'cag-platform',
      LINKED_MODULE_DIR = 'linked_modules',
      WORKSPACE_CONFIG_FILE = 'workspace.json',
      ROOT_DIR = 'references';

  var localAppData = process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + '/Library/Preferences' : process.env.HOME + '/.local/share');

  var workspacePath = Slash(Path.join(localAppData, CAG_PLATFORM_DIR, WORKSPACE_DIR, WORKSPACE_CONFIG_FILE));
  var self = this;

  this.setup = function (app) {
    var dirname = Slash(Path.dirname(cwd));

    return Fs.pathExists(workspacePath).then(function (exists) {
      if (exists) {
        return Fs.readJson(workspacePath);
      }
    }).then(function (spaces) {
      if (!spaces) {
        spaces = [];
      }

      var space = spaces.find(function (each) {
        return each.location == dirname;
      });

      if (!space) {
        spaces.push({
          location: dirname,
          reference: new Date().valueOf()
        });
      } else {
        space = Object.assign(space, app);
      }

      return Fs.outputJson(workspacePath, spaces, { spaces: '\t' }).then(function () {
        return space;
      });
    });
  };
}
/* end:inject:commons */
var commands = {};
/* inject:commands:js */
function About() {
  this.execute = function (packageJson, options) {
    Figlet.text(cagPackageJson.description, {
      font: 'Standard',
      horizontalLayout: 'universal smushing',
      verticalLayout: 'universal smushing'
    }, function (err, data) {
      data = data + '\n' + 'Version: ' + cagPackageJson.version;
      data = data + '\n' + 'Copyright: ' + cagPackageJson.author.name;
      console.log(data);
    });
  };

  this.isRequiredSettingFile = function () {
    return false;
  };
}

commands.about = new About();
function Setup() {
  var self = this;

  this.execute = function (packageJson, options) {
    var app = {};
    app[packageJson.name] = [];

    return workspace.setup(app).then(function (space) {
      logger.info('Completed setting workspace!');
    });
  };

  //TODO
  this.detectLocalModules = function (dependencies, done) {
    var modules = [];

    Object.keys(dependencies).forEach(function (key) {
      if (dependencies[key] && dependencies[key].local) {
        modules.push(key);
      }
    });

    if (modules.length == 0) {
      done(null, []);
    } else {
      var parentPath = Slash(Path.dirname(cwd));
      logger.info("Detecting local modules at '%s' directory ...", parentPath);

      Async.concat(modules, function (module, next) {
        var modulePath = Slash(Path.join(parentPath, module));

        return Fs.pathExists(modulePath + '/package.json').then(function (exists) {
          if (!exists) {
            logger.warn("Can not found module '%s'", module);
            next(null);
          } else {
            next(null, module);
          }
        });
      }, function (err, modules) {
        if (err) {
          done(err);
        } else {
          done(null, modules);
        }
      });
    }
  };

  this.isRequiredSettingFile = function () {
    return true;
  };
}

commands.setup = new Setup();
/* end:inject:commands */
var workspace = new CagWorkspace(),
    cagPackageJson = require('../package.json'),
    cwd = Slash(process.cwd()),
    appYmlPath = cwd + '/src/resources/application.yaml',
    bootstrapYmlPath = cwd + '/src/resources/bootstrap.yaml',
    argv = require('minimist')(process.argv.slice(2)),
    cli = new Liftoff({
  name: 'ops'
});

cli.launch({
  cwd: argv.cwd,
  configPath: argv.myappfile,
  require: argv.require,
  completion: argv.completion
}, invoke);

function invoke(env) {
  var appPackageJsonPath = cwd + '/package.json',
      command = argv._[0],
      executor = commands[command];

  if (executor) {
    if (executor.isRequiredSettingFile()) {
      return Fs.pathExists(appPackageJsonPath).then(function (exists) {
        if (!exists) {
          throw 'SettingException: Setting file \'package.json\' does not exist!';
        } else {
          return Fs.readJson(appPackageJsonPath).then(function (packageJson) {
            return executor.execute(packageJson, argv);
          });
        }
      }).catch(function (err) {
        logger.error(err);
      });
    } else {
      return executor.execute(cagPackageJson, argv);
    }
  } else {
    logger.error('\'CommandException: ' + command + '\' does not exist!');
  }
}