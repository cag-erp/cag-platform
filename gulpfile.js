var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({
  pattern : [ '*' ],
  overridePattern : true,
  scope : [ 'devDependencies' ]
});

gulp.task('cleanup', function () {
  return gulp.src('dist/**', {read: false})
    .pipe(plugins.clean({force: true}));
});

gulp.task('build', function () {
  return gulp.src('dist/**/*.js')
    .pipe(plugins.babel({
      presets: ['env']
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('inject', function(){
  var target = gulp.src('src/index.js');
  var buildSources = gulp.src(['src/commands/**/*.js'], {read: true});
  var commonSources = gulp.src(['src/commons/**/*.js'], {read: true});
  
  return target.pipe(
      plugins.inject(buildSources, {
        relative: true,
        starttag: '/* inject:commands:js */',
        endtag: '/* end:inject:commands */',
        transform: function (filePath, file) {
          return file.contents.toString('utf8');
        }
      })
   )
   .pipe(
       plugins.inject(commonSources, {
         relative: true,
         starttag: '/* inject:commons:js */',
         endtag: '/* end:inject:commons */',
         transform: function (filePath, file) {
           return file.contents.toString('utf8');
         }
       })
   )
   .pipe(gulp.dest('dist'));
});

gulp.task('watch', function () {
  return plugins.watch('src/**/*.*', gulp.series('cleanup', 'inject', 'build'));
});

gulp.task('start', gulp.series('cleanup', 'inject', 'build', 'watch'));